import React from 'react';
import './LoginInput.css';

export interface LoginInputProps {
  type: 'email' | 'password';
  placeholder: string;
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

function LoginInput(props: LoginInputProps) {
  const { type, placeholder, value, onChange } = props;

  return (
    <input
      type={type}
      className="login-input"
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  );
}

export default LoginInput;