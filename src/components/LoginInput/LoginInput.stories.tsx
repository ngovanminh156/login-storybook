import type { StoryObj, Meta } from '@storybook/react';
import LoginInput from './LoginInput';

// const meta = {
//   title: 'Atomic/LoginInput',
//   component: LoginInput,
  
// } satisfies Meta<typeof LoginInput>;


const meta: Meta<typeof LoginInput> = {
  title: 'Atomic/LoginInput',
  component: LoginInput,  
  tags: ['autodocs'],

}; 

export default meta;
type Story = StoryObj<typeof LoginInput>;


export const EmailInput: Story = {
  args: {
  type: 'email',
  placeholder: 'Enter your email',
  value: '',
  onChange: () => {},
},
};

export const PasswordInput: Story = {
  args: {
  type: 'password',
  placeholder: 'Enter your password',
  value: '',
  onChange: () => {},
},
};