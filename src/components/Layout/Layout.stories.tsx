import React from 'react';
import { StoryObj, Meta } from '@storybook/react';
import CustomLayout from './Layout';
import Layout from './Layout';

const meta: Meta<typeof Layout> = {
    title: 'Page/Layout',
    component: Layout,
    tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Layout>;

export const HeaderContentFooter: Story = {
    args: {
        theme: 'dark'
    }
}