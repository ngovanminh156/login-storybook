import { createGlobalStyle } from 'styled-components';
import { Layout } from 'antd';
import styled from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: 'Open Sans', sans-serif;
  }
`;

const { Header, Content, Footer } = Layout;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`;

const StyledHeader = styled(Header)`
  background-color: #001529;
  color: #fff;
`;

const StyledContent = styled(Content)`
  padding: 50px;
`;

const StyledFooter = styled(Footer)`
  text-align: center;
`;

export {
  GlobalStyle,
  StyledLayout,
  StyledHeader,
  StyledContent,
  StyledFooter
};