import { Select } from "antd";
import "./Select.css";

interface Option {
  value: string;
  label: string;
}

interface SelectProps {
  mode: "multiple" | "tags";
  options: Option[];
  value?: string;
  onChange?: (value: string) => void;
  placeholder?: string;
}

function CustomSelect(props: SelectProps) {
  const { mode, options, value, onChange, placeholder } = props;

  return (
    <Select
      mode={mode}
      className="custom-select"
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      // options={options}
      
    >
      {options.map((option) => (
        <Select.Option key={option.value} value={option.value}>
          {option.label}
        </Select.Option>
      ))}
    </Select>
  );
}

export default CustomSelect;
