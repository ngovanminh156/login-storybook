import React from "react";
import { StoryObj, Meta } from "@storybook/react";
import CustomSelect from "./Select";

// export default {
//   title: 'Atomic/CustomSelect',
//   component: CustomSelect,
// } as Meta;

// const Template: Story<SelectProps> = (args) => <CustomSelect {...args} />;

const meta: Meta<typeof CustomSelect> = {
  title: "Atomic/Select",
  component: CustomSelect,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof CustomSelect>;
//

export const SingleSelect: Story = {
  args: {
    options: [
      { value: "option1", label: "Option 1" },
      { value: "option2", label: "Option 2" },
      { value: "option3", label: "Option 3" },
    ],
    placeholder: "Select an option",
    //   mode: '',
  },
};

export const MultipleSelect: Story = {
  args: {
    options: [
      { value: "option1", label: "Option 1" },
      { value: "option2", label: "Option 2" },
      { value: "option3", label: "Option 3" },
    ],
    placeholder: "Select options",
    mode: "multiple",
  },
};
