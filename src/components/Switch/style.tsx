import styled from 'styled-components';

export const AntdSwitch = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;