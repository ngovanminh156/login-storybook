import React from "react";
import { Switch as AntdSwitch } from "antd";
// import {}

interface CustomSwitchProps {
  loading: boolean;
  checked?: boolean;
  checkedChildren: string;
  unCheckedChildren: string;
  disabled?: boolean;
  size?: "default" | "small";
  onChange?: (checked: boolean) => void;
}

function CustomSwitch(props: CustomSwitchProps) {
  const { loading, checked, checkedChildren, unCheckedChildren, disabled, size = 'default', onChange } = props;

  return (
    <AntdSwitch
      loading={loading}
      checked={checked}
      checkedChildren={checkedChildren}
      unCheckedChildren={unCheckedChildren}
      disabled={disabled}
      size={size}
      onChange={onChange}
    />
  );
}

export default CustomSwitch;
