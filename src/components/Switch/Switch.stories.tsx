import type {StoryObj, Meta} from '@storybook/react';
import Switch from './Switch';

const meta: Meta<typeof Switch> = {
    title: 'Atomic/Switch',
    component: Switch,
    tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Switch>;

export const EnableSwitch: Story = {
    args: {
        checked: true,
        disabled: false,
    }
}

export const DisableSwitch: Story = {
    args: {
        checked: false,
        disabled: true,
    }
}

export const SmallSwitch: Story = {
    args: {
        checked: true,
        disabled: false,
        size: 'small'
    }
}

export const DefaultSwitch: Story = {
    args: {
        checked: true,
        disabled: true,
        size: 'default'
    }
}

export const LoadingSwitch: Story = {
    args: {
        checked: true,
        disabled: false,
        size: 'default',
        loading: true
    }
}

export const ChildrenSwitch: Story = {
    args: {
        checked: true,
        disabled: false,
        size: 'default',
        checkedChildren: 'Yes',
        unCheckedChildren: 'No'
    }
}