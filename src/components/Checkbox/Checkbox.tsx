import React from "react";
import { Checkbox } from "antd";
import type { CheckboxChangeEvent } from "antd/es/checkbox";

interface CheckboxProps {
//   defaultValue:   
//   options: Option[];
  checked: boolean;
  disabled: boolean;
  onChange: (checked: boolean) => void;
  indeterminate: boolean;
}

// interface Option {
//   label: string;
//   value: string;
// }

function CustomCheckbox(props: CheckboxProps) {
  const { checked, disabled, onChange, indeterminate } = props;

  return (
    <>
      <Checkbox
        // options={options}
        // defaultValue={["Pear"]}
        checked={checked}
        disabled={disabled}
        // onChange={onChange}
        indeterminate={indeterminate}
      />
    </>
  );
}

export default CustomCheckbox;
