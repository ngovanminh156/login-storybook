import type { StoryObj, Meta } from "@storybook/react";
import CustomCheckbox from "./Checkbox";

const meta: Meta<typeof CustomCheckbox> = {
  title: "Atomic/Checkbox",
  component: CustomCheckbox,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof CustomCheckbox>;

export const Basic: Story = {
  args: {
    checked: true,
    disabled: false,
    indeterminate: false,
  },
};

export const Disabled: Story = {
  args: {
    checked: true,
    disabled: true,
    indeterminate: false,
  },
};

export const Controlled: Story = {
    args: {
      checked: true,
      disabled: true,
      indeterminate: false,
    },
  };