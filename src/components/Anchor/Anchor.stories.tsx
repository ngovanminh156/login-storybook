import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import { StyledAnchor, AnchorLink } from "./style";
// import { GlobalStyle } from "./style";
import Anchor from "./Anchor";
import CustomAnchor from "./Anchor";

const meta: Meta<typeof CustomAnchor> = {
  title: "Atomic/Anchor",
  component: CustomAnchor,
  tags: ["autodocs"],
  decorators: [
    (Story) => (
      <>
        <StyledAnchor />

        <div style={{ padding: "16px" }}>
          <Story />
        </div>
      </>
    ),
  ],
};
export default meta;
type Story = StoryObj<typeof CustomAnchor>;

export const Vertical: Story = {
  args: {
    href: "https://google.com",
    title: "Google",
    direction: "vertical",
    items: [
      {
        key: "part-1",
        href: "#part-1",
        title: "Part 1",
      },
      {
        key: "part-2",
        href: "#part-2",
        title: "Part 2",
      },
      {
        key: "part-3",
        href: "#part-3",
        title: "Part 3",
      },
    ],
  },
};

export const Horizontal: Story = {
  args: {
    href: "https://google.com",
    title: "Google",
    direction: "horizontal",
    items: [
      {
        key: "part-1",
        href: "#part-1",
        title: "Part 1",
      },
      {
        key: "part-2",
        href: "#part-2",
        title: "Part 2",
      },
      {
        key: "part-3",
        href: "#part-3",
        title: "Part 3",
      },
    ],
  },
};
