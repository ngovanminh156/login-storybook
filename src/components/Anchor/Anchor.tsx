import React from "react";
// import { Anchor as AntdAnchor } from "antd";
// import styled from "styled-components";
import { StyledAnchor, AnchorLink } from "./style";


interface AnchorItem {
  key: string;
  href: string;
  title: string;
}

interface AnchorProps {
  href: string;
  title: string;
  items: AnchorItem[];
  direction: 'vertical' | 'horizontal'
}

// const StyledAnchor = styled(AntdAnchor.Link)`
//   color: #1890ff;
// `;

function CustomAnchor(props: AnchorProps) {
    const{ href, title, items, direction }= props;
  return (
    <>
      {/* <StyledAnchor href={href} title={title}>
        <br />
        <br />
        {title}
      </StyledAnchor> */}
      <StyledAnchor direction={direction} >
        {items.map((item) => (
          <AnchorLink  key={item.key} href={item.href} title={item.title} />
        ))}
      </StyledAnchor>


      <div>
      <div
        id="part-1"
        style={{
          width: '100%',
          height: '100vh',
          textAlign: 'center',
          background: 'rgba(0,255,0,0.02)',
        }}
      />
      <div
        id="part-2"
        style={{
          width: '100%',
          height: '100vh',
          textAlign: 'center',
          background: 'rgba(0,0,255,0.02)',
        }}
      />
      <div
        id="part-3"
        style={{ width: '100%', height: '100vh', textAlign: 'center', background: '#FFFBE9' }}
      />
      </div>
    </>
  );
};

export default CustomAnchor;