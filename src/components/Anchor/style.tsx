import styled from "styled-components";
import { Anchor as AntdAnchor } from "antd";

export const StyledAnchor = styled(AntdAnchor)`
  width: 100%;
  background-color: #f5f5f5;
//   padding: 1rem;
  border-radius: 5px;

  .ant-anchor-ink-ball {
    background-color: #1890ff;
  }

  .ant-anchor-link-title {
    color: #1890ff;
  }

  .ant-anchor-link-active > .ant-anchor-link-title {
    color: #40a9ff;
  }
`;

export const AnchorLink = styled(AntdAnchor.Link)`
  margin-right: 2rem;

  &:hover {
    color: #40a9ff;
  }
`;