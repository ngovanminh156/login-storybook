import type { Meta, StoryObj } from '@storybook/react';

import { Button } from './LoginButton';

const meta = {
  title: 'Atomic/LoginButton',
  component: Button,
  tags: ['autodocs'],
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    primary: true,
    label: 'Login',
  },
};

export const Secondary: Story = {
  args: {
    label: 'Login',
  },
};

export const Large: Story = {
  args: {
    size: 'large',
    label: 'Login',
  },
};

export const Small: Story = {
  args: {
    size: 'small',
    label: 'Login',
  },
};
export const Tiny: Story = {
  args: {
    size: 'tiny',
    label: 'Login',
    // primary: false,
  },
};