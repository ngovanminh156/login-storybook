import React from 'react';
import './loginButton.css';

interface ButtonProps {
  primary?: boolean;
  backgroundColor?: string;
  size?: 'tiny' | 'small' | 'medium' | 'large';
  label: string;
  // shape: 'default' | 'circle' | 'round'
  onClick?: () => void;
}

export const Button = ({
  primary = false,
  // shape = 'default',
  size = 'medium',
  backgroundColor,
  label,
  ...props
}: ButtonProps) => {
  const mode = primary ? 'storybook-button--primary' : 'storybook-button--secondary';
  return (
    <button
      type="button"
      // shape={shape}
      className={['storybook-button', `storybook-button--${size}`, mode].join(' ')}
      style={{ backgroundColor }}
      {...props}
    >
      {label}
    </button>
  );
};
