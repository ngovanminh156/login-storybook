import React from "react";
// import { Icon as AntdIcon } from "@ant-design/icons";
// import type { IconProps } from "@ant-design/icons";
import { Space } from 'antd';
import {
  HomeOutlined,
  LoadingOutlined,
  SettingFilled,
  SmileOutlined,
  SyncOutlined,
} from "@ant-design/icons";

interface IconProps {
  rotate: number;
  spin: boolean;
  color: {
    options: [
      "primary",
        "secondary",
        "warning",
        "error",
        "info",
        "success",
        "action",
    ],
  },
  };


function CustomIcon(props: IconProps) {
  const { rotate, spin, color } = props;

  return (
    <>
      <Space>
    <HomeOutlined spin={spin}/>
    <SettingFilled />
    <SmileOutlined />
    <SyncOutlined spin={spin} />
    <SmileOutlined rotate={rotate} />
    <LoadingOutlined />
  </Space>
    </>
  );
}

export default CustomIcon;
