import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import CustomIcon from "./Icon";

const meta: Meta<typeof CustomIcon> = {
    title: "Atomic/Icon",
  component: CustomIcon,
  tags: ["autodocs"],
}
export default meta;
type Story = StoryObj<typeof CustomIcon>;


export const Basic: Story = {
    args: {
        rotate: 180,
        spin: true
    }
};

export const Outlined: Story = {
    args: {
        rotate: 180,
        spin: true
    }
};