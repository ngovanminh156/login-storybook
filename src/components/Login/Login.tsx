import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import Button from "@mui/material/Button";
import Input from "@mui/material/Input";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { Navigate } from "react-router-dom";

interface LoginFormValues {
  email: string;
  password: string;
}

const LoginSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email!").required("Email is required!"),
  password: Yup.string().required("Password is required!"),
});

const Login: React.FC = () => {
  const [isLoggedIn, setLoggedIn] = React.useState(false);
  const [showSuccessMessage, setShowSuccessMessage] = React.useState(false);


  if (isLoggedIn) {
    return (
    <Navigate to="/dashboard" />
    );
  }

  return (
    <Container maxWidth="sm" style={{ marginTop: "50px" }}>
      <Typography variant="h4" align="center" gutterBottom>
        Login
      </Typography>
      {showSuccessMessage && (
        <div style={{ textAlign: "center", color: "green" }}>
          Login success!
        </div>
      )}
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={LoginSchema}
        onSubmit={(values: LoginFormValues, { setSubmitting }) => {
          setTimeout(() => {
            setSubmitting(false);
            setLoggedIn(true);
            setShowSuccessMessage(true);
            setTimeout(() => {
              setShowSuccessMessage(false);
            }, 2000);
          }, 500);
        }}
      >
        {({ errors, touched, isSubmitting }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Field
                  id="outlined-error-helper-text"
                  name="email"
                  placeholder="Email"
                  as={Input}
                  fullWidth
                  style={{ marginBottom: "10px" }}
                />
                {/* <ErrorMessage name="email" /> */}
                {errors.email && touched.email && (
                  <div style={{ color: "red" }}>{errors.email}</div>
                )}
              </Grid>
              <Grid item xs={12}>
                <Field
                  name="password"
                  type="password"
                  placeholder="Password"
                  as={Input}
                  fullWidth
                  style={{ marginBottom: "10px" }}
                />
                {/* <ErrorMessage name="password" /> */}
                {errors.password && touched.password && (
                  <div style={{ color: "red" }}>{errors.password}</div>
                )}
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={isSubmitting}
                  fullWidth
                >
                  Login
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default Login;
